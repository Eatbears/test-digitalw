import React from 'react'
import {
    SnackbarContent,
    Snackbar,
    withStyles,
    IconButton,
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { connect } from 'react-redux'
import store from '../../store'

const styles = () => ({
    snackbarContent: {
        backgroundColor: 'red',
        marginTop: 20,
    },
})
// комопнент, который показывает ошибку, используется во многих компонентах, когда пользователю надо показать понятное сообщение об ошибке(не хватка прав на действие, ошибка при загрузке данных и т.д.)
class ShowError extends React.Component {
    // использую стейт, а можно закрываться на action, я ведь через 5 секунд еще одно сообщение отправляю.
    state = {
        open: false,
    }

    componentDidUpdate(prevState) {
        if (
            this.state.open !== this.props.errorSystemResponseState.status &&
            prevState.open !== this.props.errorSystemResponseState.status
        ) {
            this.setState(
                { open: this.props.errorSystemResponseState.status },
                () => {
                    if (this.props.errorSystemResponseState.status === false) {
                        this.closeModal()
                    }
                }
            )
        }
    }

    closeModal = () => {
        this.setState({ open: false })
        let timers = store.getState().timersStoreState
        Object.keys(timers).map(index => { // очистка таймеров, при закрытии
            let timer = timers[index]
            clearTimeout(timer)
        })
    }

    render() {
        const { classes, errorSystemResponseState } = this.props
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
                open={this.state.open}
                // autoHideDuration={ 5000 }
                onClose={this.closeModal}>
                <SnackbarContent
                    className={classes.snackbarContent}
                    message={errorSystemResponseState.data}
                    action={
                        <IconButton onClick={this.closeModal}>
                            <CloseIcon />
                        </IconButton>
                    }
                />
            </Snackbar>
        )
    }
}

const mapStateToProps = state => {
    return {
        errorSystemResponseState: state.errorSystemResponseState,
    }
}

export default connect(mapStateToProps)(withStyles(styles)(ShowError))
