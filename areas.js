import {
    LOADING_AREAS,
    LOADED_AREAS,
    FAIL_LOAD_AREAS,
} from './areasDistricts'

const initialState = {
    loading: false,
    areas: [],
    errorMsg: '',
    error: false,
}
// редьюсер для статусов участков(ими пользуется страница, на которой участки рисуются). state иммутабелен, есть обработки ошибок. 
// Помимо указанной страницы, этой информацией пользуется еще много других компонентов, поэтому здесь в state есть параметр loading, errorMsg, 
// другие компоненты используют loading для показа лоадера и errorMsg, в случае, если произошла ошибка загрузки.
export default (state = initialState, action) => {
    switch (action.type) {
        case LOADING_AREAS:
            return {
                ...state,
                loading: true,
            }
        case LOADED_AREAS:
            return {
                ...state,
                areas: action.payload.areas,
                loading: false,
            }
        case FAIL_LOAD_AREAS:
            return {
                ...state,
                errorMsg: action.payload.errorMsg,
                loading: false,
                error: true,
            }
        default:
            return state
    }
}
