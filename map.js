import React from 'react'

import { connect } from 'react-redux'

import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'

import { withStyles } from '@material-ui/core'

import ListAreas from './listareas'
import MapAreas from './map'
import SearchInput from '../../../common/searchinput'
import Calendar from '../../../common/calendar'
import store from '../../../store'
import { loadingSchedules } from '../../../actions/shedules'

const styles = () => ({
    cardWrapper: {
        height: '87vh',
    },
    mapWrapper: {
        height: '73vh',
    },
    searchInputWrapper: {
        marginBottom: '20px',
    },
})

class Monitoring extends React.Component {
    state = {
        collection: null,
        departmentId: '',
        statuses: null,
    }

    componentDidMount() {
        this.drawMap(
            this.props.districtsStatusesState[this.props.departmentId] // districtsStatusesState(массив) - в нем хранится информация о участках и их статусах, есть два отдела,
            // каждый отдел - элемент массива
        )
        store.dispatch({ // формирование хлебных крошек, с ссылками. "главная" и "отделы" ведут на один и тот же путь, это потому, что заказчик отказался от реализации страницы "главная",
        // когда я убрал из хлебных крошек этот пункт, некоторым женщинам стало неудобно и они попросили, чтобы он перекидывал на ту же страницу, что и отделы. 
            type: 'ADD_BREADCRUMB',
            payload: {
                Главная: '/departments',
                Отделы: '/departments',
                Мониторинг: `/${this.props.departmentId}/monitoring`,
            },
        })
        this.props.loadingSchedules(this.props.departmentId) // загрузка расписания разносок газет, расписание необходимо для детальных страниц участков,
        // на каждый отдел свои расписания, решил грузить здесь, т.к. эта страница отображается сразу после выбора отдела.
    }

    componentDidUpdate(prevProps) { 
        /*
        поэтому я проверяю, изменились ли статусы у каких-либо участков в конкретном отделе(по this.props.departmentId), участков может прийти сразу несколько.
        */
        
        if ( 
            this.props.districtsStatusesState[this.props.departmentId] !== 
            prevProps.districtsStatusesStatep[this.props.departmentId]
        ) {
            this.drawMap( // если участки изменились, то перерисовываю карту с нужным отделом 
                this.props.districtsStatusesState[
                    this.props.departmentId
                ]
            )
        }
    
    }

    drawMap = arrStatuses => {
        const self = this
        let districtCoords = this.props.districtsCoordsState // координаты участков для карты
        let CollectionPolygonsGen // коллекция полигонов
        let prevPolygon
        ymaps.ready(init)

        function init() {
            CollectionPolygonsGen = new ymaps.GeoObjectCollection({}, null)

            Object.keys(districtCoords).map((dis) => {
                let disc = districtCoords[dis]
                var myPolygon = new ymaps.Polygon(
                    [ disc ],
                    {
                        hintContent: 'Участок ' + dis,
                    },
                    {
                        fillColor: '#cc0808',
                        strokeColor: '#0000FF',
                        strokeStyle: 'solid',
                        strokeWidth: 1,
                        opacity: 0.5,
                    }
                )
                let colorDistrict = ''
                if (arrStatuses[dis] !== undefined) { // выдаем цвет полигону в зависимости от статуса
                    if (arrStatuses[dis] === 'EDS_GOOD') {
                        colorDistrict = '#4caf50'
                        myPolygon.options.set('fillColor', '#4caf50')
                    } else if (arrStatuses[dis] === 'EDS_MODERATE') {
                        colorDistrict = '#ffcd07'
                        myPolygon.options.set('fillColor', '#ffcd07')
                    } else if (arrStatuses[dis] === 'EDS_BAD') {
                        colorDistrict = '#f44336'
                        myPolygon.options.set('fillColor', '#f44336')
                    } else {
                        colorDistrict = '#2196f3'
                        myPolygon.options.set('fillColor', '#2196f3')
                    }
                }
                // задаем параметры полигонам
                myPolygon.properties.set('id', dis) 
                myPolygon.properties.set({
                    name: dis, 
                })
                myPolygon.options.set({
                    labelLayout: '{{properties.name}}',
                    labelPermissibleInaccuracyOfVisibility: 4,
                    labelTextSize: { '3_6': 12, '7_18': 14 },
                    labelDefaults: 'dark',
                    fillColor: colorDistrict,
                    strokeColor: '#0000FF',
                    strokeStyle: 'solid',
                    strokeWidth: 1,
                    opacity: 0.5,
                })
                // здесь добавлен листенер, который позволяет кликать на участки, и раньше при клике на участок на карте, можно было перейти на детальную страницу участка, но потом это было убрано
                myPolygon.events.add([ 'click' ], () => {
                    store.dispatch({
                        type: 'CHANGE_AREA',
                        payload: myPolygon.properties._data.id,
                    })

                    if (prevPolygon !== undefined) {
                        prevPolygon.options.set('opacity', '0.5')
                        myPolygon.options.set('opacity', '1')
                        prevPolygon = myPolygon
                    }
                })
                CollectionPolygonsGen.add(myPolygon)
                prevPolygon = myPolygon
            })
            self.setState({
                collection: CollectionPolygonsGen,
            })
        }
    }

    render() {
        const { classes, history } = this.props // history - глобальный параметр, по которому часть приложения переходила в "исторический" режим, и можно было переходя по определенным страницам, смотреть информацию за прошлый день. То есть, выбрали 01.01.2020, и переходя на разные страницах видим все, что связано с 01.01.2020
        const { collection } = this.state
        return (
            <div className='wrapper-monitoring'>
                <div className={classes.cardWrapper}>
                    <div className={classes.searchInputWrapper}>
                        <Card
                            style={{
                                padding: '10px',
                            }}>
                            <SearchInput
                                history={history} 
                                departmentId={this.props.departmentId}
                                go
                            />
                        </Card>
                    </div>
                    <Grid container spacing={40}>
                        <Grid item xs={3}>
                            <Grid
                                direction='column'
                                justify='space-between'
                                container>
                                <Card>
                                    <div className='listareas-wrapper'>
                                        <ListAreas
                                            departmentId={this.props.departmentId}
                                        />
                                    </div>
                                    <div className='calendar-wrapper'>
                                        <Calendar />
                                    </div>
                                </Card>
                            </Grid>
                        </Grid>
                        <Grid item xs={9}>
                            <Card className={classes.mapWrapper}>
                                {collection !== null ? ( // в случае если коллекции нет, рисую простую карту. От лоадера отказались
                                    <MapAreas collection={collection} /> 
                                ) : (
                                    <MapAreas /> 
                                )}
                            </Card>
                        </Grid>
                    </Grid>
                </div>
            </div>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        loadingSchedules: departmentId =>
            dispatch(loadingSchedules(departmentId)), 
    }
}

const mapStateToProps = state => {
    return {
        districtsCoordsState: state.districtsCoordsState, // координаты участков
        districtsStatusesState: state.districtsStatusesState,// статусы участков
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(Monitoring))
