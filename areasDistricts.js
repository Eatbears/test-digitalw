import Fetchdata from '../fetch'
import { showErrorWithTimeout } from './errorCatcher'

export const LOADING_AREAS = 'LOADING_AREAS'
export const LOADED_AREAS = 'LOADED_AREAS'
export const FAIL_LOAD_AREAS = 'FAIL_LOAD_AREAS'

// action для загрузки статусов участков, я использую middleware - redux-thunk, 

export function loadingAreas(departmentId) {
    return dispatch => {
        dispatch({
            type: LOADING_AREAS,
        })
        Fetchdata.get(`/${departmentId}/areas`, response => {
            if (response.status === 200) {
                dispatch({
                    type: LOADED_AREAS,
                    payload: {
                        areas: response.data,
                    },
                })
            } else {
                dispatch(
                    // компонент, который принимает сообщение об ошибке и отрисовывается, он глобален. Возникает как всплывающее окно
                    showErrorWithTimeout('Не удалось загрузить список участков') 
                )
                dispatch({
                    type: FAIL_LOAD_AREAS,
                    payload: {
                        errorMsg: 'Не удалось загрузить список участков', // текст ошибки, чтобы компонент мог что-то написать
                    },
                })
            }
        })
    }
}
