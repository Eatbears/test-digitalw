function showErrorMessage(data, status) {
    return { type: 'ERROR_SYSTEM', payload: { data, status } }
}

function hideErrorMessage(data, status) {
    return { type: 'ERROR_SYSTEM', payload: { data, status } }
}

function timeOutWithDefault(timer) {
    return { type: 'TIMER_STATUS', payload: { timer } }
}
// action, котороый отправляет сообщение об ошибке 
export function showErrorWithTimeout(data) {
    return function(dispatch) {
        dispatch(showErrorMessage(data, true))
        let timer = setTimeout(() => {
            dispatch(hideErrorMessage('', false))
        }, 5000)
        dispatch(timeOutWithDefault(timer))
    }
}
